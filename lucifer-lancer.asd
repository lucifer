
(defpackage lucifer-lancer-asd
  (:use :cl :asdf))

(in-package lucifer-lancer-asd)

(defsystem lucifer-lancer
  :name "Lucifer.Lancer"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "A library for SVO-or-SOV language syntax sugar"
  :pathname #-asdf2 (merge-pathnames "src/lancer/" *load-truename*)
            #+asdf2 "src/lancer/"
  :depends-on (:lucifer-lutilities)
  :components ((:file "lancer")))
