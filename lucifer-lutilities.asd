
(defpackage lucifer-lutilities-asd
  (:use :cl :asdf))

(in-package lucifer-lutilities-asd)

(defsystem lucifer-lutilities
  :name "Lucifer.Lutilities"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "General purpose utility macros and functions"
  :depends-on (:trivial-features)
  :pathname #-asdf2 (merge-pathnames "src/lutilities/" *load-truename*)
            #+asdf2 "src/lutilities/"
  :components ((:file "lutilities")))
