
(defpackage lucifer-lukism-asd
  (:use :cl :asdf))

(in-package lucifer-lukism-asd)

(defsystem lucifer-lukism
  :name "Lucifer.Lukism"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "A library for assembling and disassembling code"
  :depends-on (:lucifer-lancer :lucifer-lamack :lucifer-lutilities
			       :cl-unification)
  :pathname #-asdf2 (merge-pathnames "src/lukism/" *load-truename*)
            #+asdf2 "src/lukism/"
  :components ((:file "package")
;;	       (:module "isa" :depends-on ("package")
;;			:components
			(:file "isa/x86" :depends-on ("package"));;)
	       (:file "lukism" :depends-on ("package" "isa/x86"))))
