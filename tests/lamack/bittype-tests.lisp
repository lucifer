(require 'rt)
(require 'trivial-features)
(require 'lucifer-lamack)

(in-package :cl-user)
(defpackage lucifer.lamack.bittype.test
  (:use :cl :regression-test :bittype))

(in-package :lucifer.lamack.bittype.test)

(lancer:enable-lancer-syntax)
(lancer:register-builtin-arithmetic)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (bittype:register-builtin-bittypes)
  )

(defmacro deftest-with-expr (name test expr)
  (let ((expr-result (eval expr)))
    `(deftest ,name ,test ,expr-result)))

(deftest struct.bittype
    (format nil "~a" (struct-bittype test ((:u8 first)(:i32 second))))
    "#<BITTYPE-STRUCT TEST(U64){@0: FIRST<U8>, @4: SECOND<I32>}>")

(deftest union.bittype
    (format nil "~a" (union-bittype test ((:u8 first)(:i32 second))))
    "#<BITTYPE-STRUCT TEST(U32){@0: FIRST<U8>, @0: SECOND<I32>}>")

(deftest array.bittype
    (format nil "~a" (array-bittype :u8 3))
    "#<BITTYPE-ARRAY U8[3](U24)>")

(lancer:disable-lancer-syntax)

(eval-when (:load-toplevel :compile-toplevel :execute)
  (do-tests))
