(require 'rt)
(require 'lucifer-lamack)

(in-package :cl-user)
(defpackage lucifer.lamack.bitblock.test
  (:use :cl :regression-test :bitblock))

(in-package :lucifer.lamack.bitblock.test)

(lancer:enable-lancer-syntax)

(lancer:register-builtin-arithmetic)

(deftest bitblock-create.1 (format nil "~a" (bitblock 0)) "#<BITBLOCK 0[]>")
(deftest bitblock-create.2 (format nil "~a" (bitblock 2))
  "#<BITBLOCK 2[00 00]>")

(deftest bitblock-create.3 (format nil "~a" (bitblock-with-items 2 3))
  "#<BITBLOCK 2[02 03]>")

(deftest bitblock-write-byte
    (let ((bt (bitblock 5)))
      ([bt ][ write-byte 0 #xBB ])
      (format nil "~a" bt)) "#<BITBLOCK 5[BB 00 00 00 00]>")

(deftest bitblock-read-byte
    (let ((bt (bitblock 5)))
      ([bt ][ write-byte 0 #xBB ])
      ([bt ][ read-byte 0 ])) #xBB)

(deftest bitblock-memcpy
    (let ((bt (bitblock 5))
	  (bt2 (bitblock 4)))
      ([bt ][ write-byte 2 #xCC ])
      ([bt ][ write-byte 3 #xBB ])
      ([bt2 ][ memcpy bt ])
      (format nil "~a" bt2)) "#<BITBLOCK 4[00 00 CC BB]>")
	  

(deftest bitblock-length
    (let ((bt (bitblock 5)))
      ([bt ][ length ])) 5)


(deftest bitblock-concatenate
    (let ((bt (bitblock 5))
	  (bt2 (bitblock 5))
	  (bitblock:*bitblock-max-print-size* 20))
      ([bt ][ assign-fixnum #xAABBCCDDEE ':big-endian 0 ])
      ([bt2 ][ assign-fixnum #xCCDDEE ':little-endian 2 ])
      (format nil "~a" ([bt ][ concatenate bt2 ])))
  "#<BITBLOCK 10[AA BB CC DD EE 00 00 EE DD CC]>")


(deftest bitblock-assign
    (let ((bt (bitblock 5)))
      ([bt ][ assign-fixnum #xAABBCCDDEE ':big-endian 0 ])
      (format nil "~a" bt)) "#<BITBLOCK 5[AA BB CC DD EE]>")

(lancer:disable-lancer-syntax)

(eval-when (:load-toplevel :compile-toplevel :execute)
  (let ((bitblock::*bitblock-item-bitsize* 8))
    (do-tests))
  )
