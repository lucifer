(require 'rt)
(require 'lucifer-lancer)

(in-package :cl-user)
(defpackage lucifer.lancer.test
  (:use :cl :regression-test))

(in-package :lucifer.lancer.test)


(lancer:enable-lancer-syntax)

(lancer:register-builtin-arithmetic)

(deftest builtin-arithmetic ([ (+ 2 3) ][ * 5 6 ]) 150)

(defclass foo-class ()
    ((foo-int :initform 56))
  )

(lancer:define-lancer-method (foo-class get-int)
    foo-class-get-int ((foo-object foo-class) val)
    (+ val(slot-value foo-object 'foo-int)))

(deftest lancer-method ([ (make-instance 'foo-class) ][ get-int 4 ]) 60)

(lancer:disable-lancer-syntax)
(lancer:enable-lancer-syntax t)

(deftest lancer-method-sov ([ (make-instance 'foo-class) ][ 3 get-int ]) 59)

(lancer:disable-lancer-syntax)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (rt:do-tests)
  )
