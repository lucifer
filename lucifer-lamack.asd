
(defpackage lucifer-lamack-asd
  (:use :cl :asdf))

(in-package lucifer-lamack-asd)

(defsystem lucifer-lamack
  :name "Lucifer.Lamack"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "A library for bit twiddling"
  :depends-on (:lucifer-lancer :lucifer-lutilities :trivial-features)
  :pathname #-asdf2 (merge-pathnames "src/lamack/" *load-truename*)
            #+asdf2 "src/lamack/"
  :components ((:file "bitblock")
	       (:file "bittype" :depends-on ("bitblock"))
	       (:file "lamack" :depends-on ("bitblock" "bittype"))))
