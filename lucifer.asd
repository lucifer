
(defpackage lucifer-asd
  (:use :cl :asdf))

(in-package lucifer-asd)

(defsystem lucifer
  :name "Lucifer"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "A library for building Non-lisp things"
  :depends-on (:lucifer-lancer :lucifer-lamack))

