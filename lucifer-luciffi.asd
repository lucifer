
(defpackage lucifer-luciffi-asd
  (:use :cl :asdf))

(in-package lucifer-luciffi-asd)

(defsystem lucifer-luciffi
  :name "Lucifer.Luciffi"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "A cffi hack which brings extended structure support."
  :depends-on (:lucifer-lutilities :trivial-features :cffi)
  :pathname #-asdf2 (merge-pathnames "src/luciffi/" *load-truename*)
            #+asdf2 "src/luciffi/"
  :components ((:file "luciffi")))
