(require 'trivial-features)
(require 'cffi)

(in-package cl-user)

(defpackage lucifer.luciffi
  (:nicknames luciffi)
  (:use :cl :cffi :lutilities)
  )

(in-package luciffi)

(do-external-symbols (s (find-package 'cffi))
  (export s *package*))

(defclass foreign-type-inline (cffi::enhanced-foreign-type)
  ((actual-type
    ;; The FOREIGN-TYPE instance this type is an alias for.
    :initarg :actual-type
    :accessor actual-type
    :initform (error "Must specify an ACTUAL-TYPE."))
   (block-type
    :initarg :block-type
    :accessor block-type
    :initform :unsigned-int))
  (:documentation "A type that is a place-holder for actual data."))


(defmethod cffi::canonicalize ((type foreign-type-inline))
  "Return the built-in type keyword for TYPE."
  (block-type type))

(defmethod cffi::aggregatep ((type foreign-type-inline))
  "Return false."
  nil)

(defmethod cffi::foreign-type-size ((type foreign-type-inline))
  "Return the size in bytes of a foreign typedef."
  (foreign-type-size (block-type type)))


(defclass foreign-type-inline-header (foreign-type-inline)
  ()
  (:documentation "A type that is a place-holder for actual data."))

(defmethod cffi::foreign-type-alignment ((type foreign-type-inline-header))
  "Return the alignment of a foreign typedef."
  (foreign-type-alignment (actual-type type)))

(defmethod index ((type foreign-type-inline-header))
  1)

(defclass foreign-type-inline-padding (foreign-type-inline)
  ((index
    :initarg :index
    :accessor index
    :initform (error "Must specify an INDEX."))
   )
  (:documentation "A type that is a place-holder for actual data."))

(defmethod cffi::foreign-type-alignment ((type foreign-type-inline-padding))
  "Return the alignment of a foreign typedef."
  1)


(defmethod cffi::translate-to-foreign (object (type foreign-type-inline-header))
  (multiple-value-bind (marshalled-object param)
      (cffi::translate-to-foreign object (cffi::parse-type (actual-type type)))
    (prog1
	(if (cffi::pointerp marshalled-object)
	    (cffi::%mem-ref marshalled-object (block-type type) 0)
	    (ldb (let* ((block-size (cffi::foreign-type-size (block-type type)))
			(total-size (cffi::foreign-type-size (actual-type type))))
		   (declare (ignorable total-size))
		   #+little-endian (byte (* 8 block-size) 0)
		   #+big-endian (byte (* 8 block-size) (* 8 (- total-size block-size))))
		 marshalled-object))
      (cffi::free-translated-object marshalled-object
				    (cffi::parse-type (actual-type type)) param))))

(defmethod cffi::translate-to-foreign (object (type foreign-type-inline-padding))
  (multiple-value-bind (marshalled-object param)
      (cffi::translate-to-foreign object (cffi::parse-type (actual-type type)))
    (prog1
	(if (cffi::pointerp marshalled-object)
	    (cffi::%mem-ref marshalled-object (block-type type)
			   (* (1- (index type))
			      (cffi::%foreign-type-size (block-type type))))
	    (ldb (let* ((block-size (cffi::foreign-type-size (block-type type)))
			(total-size (cffi::foreign-type-size (actual-type type))))
		   (declare (ignorable total-size))
		   #+little-endian (byte (* 8 block-size) (* 8 (1- (index type))))
		   #+big-endian (byte (* 8 block-size) (* 8 (- total-size
							       (* (index type)
								  block-size)))))
		 marshalled-object))
      (cffi::free-translated-object marshalled-object
				    (cffi::parse-type (actual-type type)) param))))

(defmethod cffi::foreign-struct-slot-value :around (ptr (slot cffi::simple-struct-slot))
  "Return the value of a simple SLOT from a struct at PTR."
  (if (typep (cffi::parse-type (cffi::slot-type slot)) 'foreign-type-inline)
      (let* ((obj-type (cffi::slot-type slot))
	     (obj-type-foreign (cffi::parse-type obj-type)))
	(cffi::inc-pointer ptr (- (cffi::slot-offset slot)
				  (* (1- (index obj-type-foreign))
				     (cffi::foreign-type-size
				      (block-type obj-type-foreign))))))
      (call-next-method)))

(defmethod cffi::foreign-struct-slot-value-form
    :around (ptr (slot cffi::simple-struct-slot))
  "Return a form to get the value of a slot from PTR."
  (if (typep (cffi::parse-type (cffi::slot-type slot)) 'foreign-type-inline)
      (let* ((obj-type (cffi::slot-type slot))
	     (obj-type-foreign (cffi::parse-type obj-type)))
	`(cffi::inc-pointer ,ptr ,(- (cffi::slot-offset slot)
				  (* (1- (index obj-type-foreign))
				     (cffi::foreign-type-size
				      (block-type obj-type-foreign))))))
      (call-next-method)))

(defmethod (setf cffi::foreign-struct-slot-value)
    :around (value ptr (slot cffi::simple-struct-slot))
  "Set the value of a simple SLOT to VALUE in PTR."
  (if (typep (cffi::parse-type (cffi::slot-type slot)) 'foreign-type-inline)
      (progn
	(if (not (cffi:pointerp value))
	    (error "Please supply a pointer of the same type."))
	(let* ((slot-offset (cffi::slot-offset slot))
	       (obj-type (cffi::slot-type slot))
	       (obj-type-foreign (cffi::parse-type obj-type))
	       (obj-type-size (cffi::foreign-type-size (actual-type obj-type-foreign)))
	       (block-size  (cffi::foreign-type-size (block-type obj-type-foreign)))
	       (block-index (index obj-type-foreign))
	       (mem-start  (cffi::inc-pointer ptr (- slot-offset
						     (* block-size (1- block-index))))))
	  (loop for i below obj-type-size
	     do (cffi::%mem-set (cffi::%mem-ref value :char i) mem-start
				:char i))
	  value)
	)
      (call-next-method)))

(defmethod cffi::foreign-struct-slot-set-form
    :around (value ptr (slot cffi::simple-struct-slot))
  "Return a form to set the value of a simple structure slot."
  (if (typep (cffi::parse-type (cffi::slot-type slot)) 'foreign-type-inline)
      (progn
	(let* ((slot-offset (cffi::slot-offset slot))
	       (obj-type (cffi::slot-type slot))
	       (obj-type-foreign (cffi::parse-type obj-type))
	       (obj-type-size (cffi::foreign-type-size (actual-type obj-type-foreign)))
	       (block-size  (cffi::foreign-type-size (block-type obj-type-foreign)))
	       (block-index (index obj-type-foreign)))
	  `(progn
	     (if (not (cffi:pointerp ,value))
		 (error "Please supply a pointer of the same type."))
	     (loop for i below ,obj-type-size
		do (cffi::%mem-set (cffi::%mem-ref ,value :char i)
				   (cffi::inc-pointer
				    ,ptr ,(- slot-offset(* block-size (1- block-index))))
				   :char i))
	     ,value))
	)
      (call-next-method)))

(defpackage lucifer.luciffi.struct-inline-padding
  (:nicknames luciffi.struct-ext)
  )

(defun generate-interned-type-name (package symbol-name
				    &optional (index nil)
				    (dest-package
				     (find-package 'luciffi.struct-ext)))
  (intern (concatenate 'string
		       (if package
			   (concatenate 'string (package-name package) "_")
			   "")
		       symbol-name
		       (if index (format nil "@~a" index) ""))
	  dest-package)
  )

(defun generate-inline-field-sequence (field-name field-type)
  (let* ((field-name-package   (symbol-package field-name))
	 (field-name-string    (symbol-name field-name))
	 (field-type-package   (symbol-package field-type))
	 (field-type-string    (symbol-name field-type))
	 (field-type-totalsize (cffi::foreign-type-size
				field-type))
	 (field-type-intsize   (cffi::foreign-type-size
				:unsigned-int))
	 (block-type (if (zerop (mod field-type-totalsize
				     field-type-intsize))
			 :unsigned-int  :unsigned-char))
	 (block-size (cffi::foreign-type-size
		      (cffi::canonicalize-foreign-type block-type)))
	 (block-count (/ field-type-totalsize block-size))
	)
    (loop for n from 1 to block-count
       collecting (list (generate-interned-type-name
			 nil field-name-string
			 (if (= n 1) nil n) field-name-package)
			(generate-interned-type-name
			 field-type-package field-type-string
			 (if (= n 1) nil n))))
  ))

(defun ensure-inline-type-created (field-type)
  (handler-case
      (cffi::find-type-parser (generate-interned-type-name
		 (symbol-package field-type) (symbol-name field-type)
		 nil))
    (error ()
      (let* ((field-type-package   (symbol-package field-type))
	     (field-type-string    (symbol-name field-type))
	     (field-type-foreign   (cffi::parse-type
				    field-type))
	     (field-type-totalsize (cffi::foreign-type-size
				    field-type-foreign))
	     (field-type-intsize   (cffi::foreign-type-size
				    (cffi::canonicalize-foreign-type
				     :unsigned-int)))
	     (block-type (if (zerop (mod field-type-totalsize
					 field-type-intsize))
			     :unsigned-int  :unsigned-char))
	     (block-size (cffi::foreign-type-size
			  (cffi::canonicalize-foreign-type block-type)))
	     (block-count (/ field-type-totalsize block-size))
	     )
	(loop for n from 1 to block-count
	   do  (cffi::notice-foreign-type
		(generate-interned-type-name
		 field-type-package field-type-string
		 (if (= n 1) nil n))
		(if (= n 1)
		    (make-instance 'foreign-type-inline-header
				   :actual-type field-type
				   :block-type  block-type)
		    (make-instance 'foreign-type-inline-padding
				   :actual-type field-type
				   :block-type  block-type
				   :index       n))
		
		))))))

(defmacro-exported defcstruct-extended (name-and-options &body fields)
  (let ((new-fields fields))
    (cffi::discard-docstring new-fields)
    (let* ((inline-types nil)
	   (generated-fields
	    (loop for (field-name field-type) in new-fields
	       if (symbolp field-type) ;;normal case 
	       appending (list (list field-name field-type))
	       else
	       appending
		 (cond
		   ((eq (car field-type) :inline)
		    (pushnew (cadr field-type) inline-types)
		    (generate-inline-field-sequence field-name
						    (cadr field-type))
		    )
		   (t (list (list field-name field-type))))
	       end))
	   )
      `(progn
	 (eval-when (:compile-toplevel :load-toplevel :execute)
	   ,@(loop for type in (reverse inline-types)
		collecting `(ensure-inline-type-created ',type))
	   )
	 (defcstruct ,name-and-options ,@generated-fields)))))



(defun mangle-lisp-name (name)
  (let ((name-package (symbol-package name))
	(name-string  (symbol-name name)))
    (intern (format nil "%_~a" name-string) name-package)))


(defun process-args-expander-normal (param-name param-type)
  (list (list param-name param-type param-name param-type)))

(defun process-args-expander-inline (param-name param-type)
  (ensure-inline-type-created param-type)
  (let* ((param-name-package (symbol-package param-name))
	 (param-name-name    (symbol-name    param-name))
	 (param-type-package (symbol-package param-type))
	 (param-type-name    (symbol-name    param-type))
	 (param-type-size    (cffi::foreign-type-size param-type))
	 (header-type-name   (generate-interned-type-name
			      param-type-package param-type-name nil))
	 (header-type        (cffi::parse-type header-type-name))
	 (block-type         (block-type header-type))
	 (block-size         (cffi::foreign-type-size block-type))
	 (block-count        (/ param-type-size block-size)))
    (loop for n from 1 to block-count
	 collecting (list param-name param-type
			  (generate-interned-type-name
			   nil param-name-name
			   (if (= n 1) nil n) param-name-package)
			  (generate-interned-type-name
			   param-type-package param-type-name
			   (if (= n 1) nil n))))))

(defun process-args-default-expander (param-name param-type)
  (list (list param-name param-type)))

(defun process-args (original-params original-types process-fn normal-fn)
  (assert (= (length original-params)(length original-types)))
  (loop for param-name in original-params
     and param-type in original-types
     if (and (consp param-type)
	     (eq (car param-type) :inline))
       appending (funcall process-fn param-name (cadr param-type))
       else
       appending (funcall normal-fn param-name param-type)
       end))

;; not done yet.
(defmacro-exported defcfun-extended-with-fn
    (name-and-options return-type pp-fn &body args)
  "Defines a Lisp function that calls a foreign function."
  (let ((docstring (when (stringp (car args)) (pop args))))
    (multiple-value-bind (lisp-name foreign-name options)
        (cffi::parse-name-and-options name-and-options)
      (let ((mangled-lisp-name (mangle-lisp-name lisp-name)))
        (if (eq (car (last args)) '&rest)
	    (let* ((arbitrary-sym (gensym))
		   (original-params (mapcar #'car (butlast args)))
		   (original-param-types (mapcar #'cadr (butlast args)))
		   (processed-args (process-args original-params original-param-types
						 #'process-args-expander-inline
						 #'process-args-expander-normal))
		   (formal-args (process-args original-params original-param-types
					      #'process-args-default-expander
					      #'process-args-default-expander))
		   
		   )
	      `(progn
		 ,(cffi::%defcfun-varargs mangled-lisp-name foreign-name return-type
					  (mapcar (lambda(x) (list (third x) (fourth x)))
						  processed-args)
					  options docstring)
		 (defun ,lisp-name (,@(mapcar (lambda (x) (first x)) formal-args)
				    &rest ,arbitrary-sym)
		   ,(funcall pp-fn
			     `(apply #',mangled-lisp-name
				     (list* ,@(mapcar (lambda (x) (first x))
						      processed-args)
					    ,arbitrary-sym)))))
	      )
	    (let* ((original-params (mapcar #'car args))
		   (original-param-types (mapcar #'cadr args))
		   (processed-args (process-args original-params original-param-types
						 #'process-args-expander-inline
						 #'process-args-expander-normal))
		   (formal-args (process-args original-params original-param-types
					      #'process-args-default-expander
					      #'process-args-default-expander))
		   )
	      `(progn
		 ,(cffi::%defcfun mangled-lisp-name foreign-name return-type
				  (mapcar (lambda(x) (list (third x) (fourth x)))
					  processed-args)
				  options docstring)
		 (defun ,lisp-name ,(mapcar (lambda (x) (first x)) formal-args)
		   ,(funcall pp-fn
			     `(,mangled-lisp-name ,@(mapcar (lambda (x) (first x))
							    processed-args)))))
	      ))))))

(defmacro-exported defcfun-extended (name-and-options return-type &body args)
  `(defcfun-extended-with-fn ,name-and-options ,return-type ,#'cl:identity ,@args))

(defun-exported foreign-reintern-slot-value (ptr class-name slot-name-to-reintern)
  (foreign-slot-value ptr class-name (intern (symbol-name slot-name-to-reintern)
					     (symbol-package class-name))))

(defun-exported (setf foreign-reintern-slot-value)
    (val ptr class-name slot-name-to-reintern)
  (setf (foreign-slot-value ptr class-name
			    (intern (symbol-name slot-name-to-reintern)
				    (symbol-package class-name))) val))

;; bitfield


(defun bitfield-extract-original-pairs (type)
  (loop for sym being the hash-key in (cffi::symbol-values type)
     using (hash-value val)
       collecting (list sym val)))

(defmacro-exported defbitfield-extended (name-and-options base-type &body masks)
  `(defbitfield ,name-and-options
     ,@(if base-type
	  nil
	  (bitfield-extract-original-pairs (cffi::parse-type base-type))) ,@masks))



(defmacro-exported with-foreign-objects-and-values ((&rest objects) &body body)
    (loop for (obj-name obj-class . init-values) in objects
          collecting `(,obj-name ',obj-class) into declaration-list
          appending (loop for (field-name value) in init-values
                                collecting (if field-name
                                                `(setf (foreign-reintern-slot-value
							,obj-name
							',obj-class
							',field-name) ,value)
                                                `(setf ,obj-name ,value)))
          into initialization-list
          finally (return `(with-foreign-objects ,declaration-list
                                ,@initialization-list
                                ,@body))))
