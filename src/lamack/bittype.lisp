;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;  Lamack.BitBlock
;;  Part of Project Lucifer
;;  by CrLF0710<crlf0710@gmail.com>

(require 'lucifer-lancer)
(require 'trivial-features)
(require 'lucifer-lutilities)

(defpackage lucifer.lamack.bittype
  (:nicknames bittype)
  (:use :cl :lucifer.lancer
	:lucifer.lamack.bitblock :lucifer.lutilities)
  )

(in-package bittype)

(lancer:enable-lancer-syntax)

(defvar +byte-bitnum+ 8)

(defstruct-exported bittype
  )


(defstruct-exported
    (bittype-object (:include bittype))
    (name nil)
  (size 0)
  (signed nil)
  (endianness 0)
  )

(define-lancer-method (bittype-object name)
    bittype-name ((bittype bittype-object))
    (bittype-object-name bittype))

(define-lancer-method (bittype-object size)
    bittype-size ((bittype bittype-object))
    (bittype-object-size bittype))

(define-lancer-method (bittype-object eltsize)
    bittype-eltsize ((bittype bittype-object))
    (bittype-object-size bittype))

(define-lancer-method (bittype-object signed)
    bittype-signed ((bittype bittype-object))
    (bittype-object-signed bittype))

(define-lancer-method (bittype-object endianness)
    bittype-endianness ((bittype bittype-object))
    (bittype-object-endianness bittype))


(defstruct-exported
    (bittype-array (:include bittype))
    (type nil)
  (count 0)
  )

(define-lancer-method (bittype-array type)
    bittype-type ((bittype bittype-array))
    (bittype-array-type bittype))

(define-lancer-method (bittype-array count)
    bittype-count ((bittype bittype-array))
    (bittype-array-count bittype))

(define-lancer-method (bittype-array name)
    bittype-name ((bittype bittype-array))
    (intern (format nil "~a[~a]"
		    (bittype-name (bittype-type bittype))
		    (bittype-count bittype))))

(define-lancer-method (bittype-array eltsize)
    bittype-eltsize ((bittype bittype-array))
    (bittype-eltsize (bittype-type bittype)))

(define-lancer-method (bittype-array size)
    bittype-size ((bittype bittype-array))
    (* (bittype-size (bittype-type bittype))
       (bittype-count bittype)))

(defstruct-exported
    (bittype-struct (:include bittype))
    (name nil)
  (size 0)
  (fields nil)
  )

(define-lancer-method (bittype-struct name)
    bittype-name ((bittype bittype-struct))
    (bittype-struct-name bittype))

(define-lancer-method (bittype-struct size)
    bittype-size ((bittype bittype-struct))
    (bittype-struct-size bittype))

(define-lancer-method (bittype-struct eltsize)
    bittype-eltsize ((bittype bittype-struct))
    (bittype-struct-size bittype))

(define-lancer-method (bittype-struct fields)
    bittype-fields ((bittype bittype-struct))
    (bittype-struct-fields bittype))

(define-lancer-method (bittype-struct field)
    bittype-struct-field ((bittype bittype-struct) name)
    (labels ((symbol-name-equal (a b)
	       (string= (symbol-name a) (symbol-name b))))
      (find name (bittype-fields bittype) :key #'second
	    :test #'symbol-name-equal)))

(defmethod print-object ((bittype bittype-object) stream)
  (print-unreadable-object (bittype stream :type 'bittype)
    (format stream
	    "~a(~:[U~;I~]~a, ~(~d~) mode)"
	    (bittype-name bittype)
	    (bittype-signed bittype)
	    (bittype-size bittype)
	    (bittype-endianness bittype)
	    )))

(defmethod print-object ((bittype bittype-array) stream)
  (print-unreadable-object (bittype stream :type 'bittype)
    (format stream
	    "~a(U~a)"
	    (bittype-name bittype)
	    (bittype-size bittype)
	    )))

(defmethod print-object ((bittype bittype-struct) stream)
  (print-unreadable-object (bittype stream :type 'bittype)
    (format stream
	    "~a(U~a){~{~a~^, ~}}"
	    (bittype-name bittype)
	    (bittype-size bittype)
	    (loop for field in (bittype-fields bittype)
		 collecting (destructuring-bind (type name offset) field
			      (format nil "@~d: ~a<~a>"
				      (/ offset +byte-bitnum+) name
				      (bittype-name type)))))))

(defvar *bittypes* (make-hash-table :test #'equal))
(defvar *bittype-bit-per-byte* 8)

(defun find-bittype (name)
  (gethash name *bittypes*)
  )

(defun register-bittype (name type)
  (setf (gethash name *bittypes*) type)
  )

(defun create-bittype (name size signed endianness)
  (let ((bittype
	 (make-bittype-object :name name :size size
			      :signed signed :endianness endianness)))
    (register-bittype name bittype)
    bittype))

(defun create-bittype-array (type count)
  (let ((bittype
	 (make-bittype-array :type type :count count)))
    bittype))

(defun create-bittype-struct (name size fields)
  (let ((bittype
	 (make-bittype-struct :name name :size size :fields nil)))
    (register-bittype name bittype)
    (setf (bittype-struct-fields bittype)
	  (loop for field in fields
	     collecting (list (find-bittype (first field))
			      (intern (symbol-name (second field)) "KEYWORD")
			      (third field)))
	  )
    bittype))

(defmacro bittype (name size signed endianness fields)
  `(create-bittype ',name ,size ,signed ,endianness ,fields)
  )

(defvar-exported *lamack-bittype-pack-size* 4)

(defun pack-bitsize ()
  (* *lamack-bittype-pack-size*
     *bittype-bit-per-byte*))

(defun create-struct-bittype (name fields)
  (let* ((offset 0)
	 (max-field-size 0)
	 (result-fields
	  (loop for field in fields
	     collecting
	       (let* ((field-name-struct (second field))
		      (field-name field-name-struct)
		      (field-type-name (first field))
		      (field-type (find-bittype field-type-name))
		      (field-size (bittype-size field-type))
		      (field-eltsize (bittype-eltsize field-type))
		      ;;align member
		      (field-align (min field-eltsize (pack-bitsize)))
		      (field-start
		       (* (ceiling (/ offset field-align)) field-align)))
		 (setf max-field-size (max max-field-size field-size))
		 (setf offset (+ field-start field-size))
		 (list field-type-name field-name field-start))))
	 (total-size (* (ceiling (/ offset (pack-bitsize))) (pack-bitsize)))
	 )
    (create-bittype-struct name total-size result-fields)))

(defun create-union-bittype (name fields)
  (let* ((offset 0)
	 (max-field-size 0)
	 (result-fields
	  (loop for field in fields
	     collecting
	       (let* ((field-name-struct (second field))
		      (field-name field-name-struct)
		      (field-type-name (first field))
		      (field-type (find-bittype field-type-name))
		      (field-size (bittype-size field-type))
		      (field-start 0))
		 (setf max-field-size (max max-field-size field-size))
		 (setf offset max-field-size)
		 (list field-type-name field-name field-start))))
	 (total-size (* (ceiling (/ offset (pack-bitsize))) (pack-bitsize)))
	 )
    (create-bittype-struct name total-size result-fields)))

(defmacro-exported array-bittype (type count)
  `(create-bittype-array (find-bittype ',type) ',count))

(defmacro-exported struct-bittype (name fields)
  `(create-struct-bittype ',name ',fields))

(defmacro-exported union-bittype (name fields)
  `(create-union-bittype ',name ',fields))


;;(defun register-enum-bittype
(defun-exported register-builtin-bittypes ()
  (let ((endianness bitblock:*bitblock-default-endianness*))
    (create-bittype ':u8  8  nil endianness)
    (create-bittype ':u16 16 nil endianness)
    (create-bittype ':u32 32 nil endianness)
    (create-bittype ':u64 64 nil endianness)
    (create-bittype ':i8  8  t   endianness)
    (create-bittype ':i16 16 t   endianness)
    (create-bittype ':i32 32 t   endianness)
    (create-bittype ':i64 64 t   endianness)
    (struct-bittype :complex ((:i32 real 0)
			      (:i32 imagine 0)))
    t
    ))



(defstruct-exported bitref
  (bittype nil)
  (bitblock nil)
  (offsetval 0)
  )

(defstruct-exported bitptr
  (bittype nil)
  (bitblock nil)
  (ptrval 0)
  )

(defun-exported bitref (bittype &optional bitblock (offsetval 0))
  (make-bitref :bittype bittype :bitblock bitblock :offsetval offsetval))

(defun-exported bitptr (bittype &optional bitblock (ptrval 0))
  (make-bitptr :bittype bittype :bitblock bitblock :ptrval ptrval))

(define-lancer-method (bitptr deref)
    bitptr-deref ((bitptr bitptr))
    (bitref (bitptr-bittype bitptr)
	    (bitptr-bitblock bitptr)
	    (bitptr-ptrval bitptr)))

(define-lancer-method (bitref addr-of)
    bitaddr-addr-of ((bitref bitref))
    (bitptr (bitref-bittype bitref)
	    (bitref-bitblock bitref)
	    (bitref-offsetval bitref)))

(define-lancer-method (bitptr -> )
    bitptr-member ((bitptr bitptr) field-name)
    (destructuring-bind (type name offset)
	([ (bitptr-bittype bitptr) ][ field field-name ])
      (declare (ignore name))
      (bitref type
	      (bitptr-bitblock bitptr)
	      (+ (bitptr-ptrval bitptr) offset))))

(define-lancer-method (bitref _ )
    bitref-member ((bitref bitref) field-name)
    (destructuring-bind (type name offset)
	([ (bitref-bittype bitref) ][ field field-name ])
      (declare (ignore name))
      (bitref type
	      (bitref-bitblock bitref)
	      (+ (bitref-offsetval bitref) offset))))
	      

(define-lancer-method (bittype malloc)
    bittype-block-malloc ((bittype bittype))
    (make-bitptr :bittype bittype :ptrval 0
		 :bitblock (bitblock-varsized (bittype-size bittype)
					      +byte-bitnum+)))

(define-lancer-method (bitptr free)
    bittype-block-free ((bitptr bitptr))
    (setf (bitblock-data (bitptr-bitblock bitptr)) nil))

;; arithmetic

(defun-exported int-to-uint (value bitsize)
  (ldb (byte bitsize 0) value))

(defun-exported uint-to-int (value bitsize)
  (if (logbitp (1- bitsize) value)
      (dpb value (byte bitsize 0) -1)
      value))

(lancer:disable-lancer-syntax)
