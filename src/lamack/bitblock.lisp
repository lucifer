;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;  Lamack.BitBlock
;;  Part of Project Lucifer
;;  by CrLF0710<crlf0710@gmail.com>

(require 'lucifer-lancer)
(require 'trivial-features)

(defpackage lucifer.lamack.bitblock
  (:nicknames bitblock)
  (:use :cl :lucifer.lancer :lucifer.lutilities)
  )

(in-package :bitblock)

(defvar-exported *bitblock-max-print-size* 5)
(defvar-exported *bitblock-item-bitsize*   32)
(defvar-exported *bitblock-default-endianness*
  #+little-endian :little-endian #+big-endian :big-endian)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; structures

(defstruct-exported bitblock
      (data (make-array 0 :element-type 'integer)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (export 'bitblock-data))

(defun-exported bitblock (size)
  (make-bitblock :data (make-array size :element-type 'integer
				   :initial-element 0)))

(define-lancer-method (bitblock varsize)
    bitblock-varsize ((bitblock bitblock))
    *bitblock-item-bitsize*)

(defmethod bitblock-item-bitmask ((bitblock bitblock))
  (1- (ash 1 (bitblock-varsize bitblock))))

(defmethod bitblock-item-bitmask ((varsize integer))
  (1- (ash 1 varsize)))

(defun-exported bitblock-with-items (&rest item-list)
  (make-bitblock
   :data (apply #'vector
		(mapcar (lambda (x)
			  (logand x (bitblock-item-bitmask
				     *bitblock-item-bitsize*)))
			item-list ))))

(defstruct-exported (bitblock-varsized (:include bitblock))
    varsize)

(define-lancer-method (bitblock-varsized varsize)
    bitblock-varsize ((bitblock bitblock-varsized))
    (bitblock-varsized-varsize bitblock))


(defun-exported bitblock-varsized
    (size &optional (varsize *bitblock-item-bitsize*))
  (make-bitblock-varsized :varsize varsize
			  :data (make-array size :element-type 'integer
					    :initial-element 0)))

(defun-exported bitblock-varsized-with-items (varsize &rest item-list)
  (make-bitblock-varsized
   :varsize varsize
   :data (apply #'vector
		(mapcar (lambda (x)
			  (logand x (bitblock-item-bitmask varsize)))
			item-list ))))

(eval-when (:load-toplevel :execute)
  (add-lancer-class-methods bitblock-varsized
			    (varsize #'bitblock-varsized-varsize)))

(eval-when (:load-toplevel :execute)
  (lancer:add-lancer-class-parent 'bitblock-varsized 'bitblock))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; methods

(defmethod print-object ((bitblock bitblock) stream)
  (print-unreadable-object (bitblock stream :type 'bitblock)
    (let ((length (length (bitblock-data bitblock))))
      (format stream (format nil "~~a[~~{~~~a,'0x~~^ ~~}~~a]"
			     (/ (bitblock-varsize bitblock) 4))
	    length
	    (reverse (loop for n from (min length
				  *bitblock-max-print-size*) downto 1
		 collect (elt (bitblock-data bitblock) (1- n))))
	    (if (> length *bitblock-max-print-size*) " ..." "")
	    ))))

(define-lancer-method (bitblock read-byte)
    bitblock-read-byte ((bitblock bitblock) index)
    (elt (bitblock-data bitblock) index)
    )

(define-lancer-method (bitblock write-byte)
    bitblock-write-byte ((bitblock bitblock) index newval)
    (setf (elt (bitblock-data bitblock) index)
	  (logand newval (bitblock-item-bitmask bitblock)))
    )

(define-lancer-method (bitblock length)
    bitblock-length ((bitblock bitblock))
    (length (bitblock-data bitblock)))

(define-lancer-method (bitblock memcpy)
    bitblock-memcpy ((target bitblock) (src bitblock)
		     &optional (target-index 0)
		     (length nil) (src-index 0))
    (setf length (min (if length length (bitblock-length src))
		      (- (bitblock-length target) target-index)
		      (- (bitblock-length src) src-index)))
    (loop for n from 1 to length 
	do (setf (elt (bitblock-data target) (+ target-index n -1))
		 (elt (bitblock-data src)    (+ src-index    n -1)))))

(define-lancer-method (bitblock concatenate)
    bitblock-concatenate (&rest bitblocks)
    (let ((new-bitblock (bitblock (loop for n in bitblocks
				     summing (bitblock-length n))))
	  (fill-ptr 0))
      (loop for n in bitblocks
	 do (progn (bitblock-memcpy new-bitblock n fill-ptr)
		   (incf fill-ptr (bitblock-length n)))
	   finally (return new-bitblock))))

(define-lancer-method (bitblock read-fixnum)
    bitblock-read-fixnum ((bitblock bitblock) &optional (endianness nil)
			  (startpos 0) (length nil))
    (if (not endianness) (setf endianness *bitblock-default-endianness*))
    (setf length (min (- (bitblock-length bitblock) startpos)
		      (or length
			  (bitblock-length bitblock))))
    (loop for i from (1- length) downto 0
       summing (ash (elt (bitblock-data bitblock) (+ startpos i))
		    (* (bitblock-varsize bitblock)
		       (if (eql endianness :little-endian)
			   i
			   (- length i 1))))))

(define-lancer-method (bitblock assign-fixnum)
    bitblock-assign-fixnum ((bitblock bitblock) newval &optional
			    (endianness nil) (startpos 0) (length nil))
    (if (not endianness) (setf endianness *bitblock-default-endianness*))
    (setf length (min (- (bitblock-length bitblock) startpos)
		      (or length
			  (bitblock-length bitblock))))
    (loop for i from (1- length) downto 0
       do (setf (elt (bitblock-data bitblock) (+ startpos i))
		(ldb (byte (bitblock-varsize bitblock)
			   (* (bitblock-varsize bitblock)
			      (if (eql endianness :little-endian)
			       i
			       (- length i 1)))) newval))))

(define-lancer-method (bitblock dump)
    bitblock-dump ((bitblock bitblock) &optional (stream *standard-output*)
		   (startpos 0) (length nil))
    (if (null length) (setf length (bitblock-length bitblock)))
    (loop for n from startpos to (min (+ startpos length)
				      (bitblock-length bitblock))
       do (write-byte (bitblock-read-byte bitblock n) stream)))
