(in-package cl-user)

(defpackage lucifer.lamack
  (:nicknames lamack)
  (:use :cl :lucifer.lamack.bitblock :lucifer.lamack.bittype))
