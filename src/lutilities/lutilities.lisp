
(in-package cl-user)

(defpackage lucifer.lutilities
  (:nicknames lutilities)
  (:use :cl))

(in-package lutilities)

(defmacro defmacro-exported (name pattern &body body)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',name))
     (defmacro ,name ,pattern ,@body)))

(eval-when (compile load eval)
  (export '(defmacro-exported)))

(defmacro defexport (defdef name &rest rest)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',name))
     (,defdef ,name ,@rest)))

(eval-when (compile load eval)
  (export '(defexport)))


;; A defun that exports it's name
(defmacro-exported defun-exported (function-spec lambda-list &body body)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',function-spec))
     (defun ,function-spec ,lambda-list ,@body)))

;; A defsetf that exports it's name
(defmacro-exported defsetf-exported (name function)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export '(setf ,name)))
     (defsetf ,name ,function)))

;; A class generation and exported function
(defmacro-exported defclass-exported (classname superclasses slots &rest options)
  "Exports the class name and all defined accessors using reflection"
  (let ((class (gensym)))  
    `(progn
       (defclass ,classname ,superclasses ,slots ,@options)
       (eval-when (:compile-toplevel :load-toplevel :execute)
	 (export ',classname)))))

;; A structure generation and exporting facility
(defmacro-exported defstruct-exported (options &rest slots)
  "Exports the structure and related accessors using reflection"
  (let (structname)
    (if (atom options)
	(setf structname options)
	(loop for option in options
	     do (if (atom option) (setf structname option))))
    `(progn
       (eval-when (:compile-toplevel :load-toplevel :execute)
	 (export ',structname))
       (defstruct ,options ,@slots))))

;; An exported generic function 
(defmacro-exported defgeneric-exported (name lambda-list &rest args)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',name))
     (defgeneric ,name ,lambda-list ,@args)))
 
;; A generic function that exports its name
(defmacro-exported defmethod-exported (method-spec lambda-list &body body)
  `(progn
     (defmethod ,method-spec ,lambda-list ,@body)
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',method-spec))))

;; A defvar that exports it's name
(defmacro-exported defvar-exported (name &rest args)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',name))
     (defvar ,name ,@args)))

;; A defconstant that exports it's name
(defmacro-exported defconstant-exported (name &rest args)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',name))
     (defconstant ,name ,@args)))

;; Used instead of defun to add an inline proclamation
(defmacro-exported defsubst (function lambda-list &body body)
  `(progn 
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (cl:proclaim '(cl:inline ,function)))
     (defun ,function ,lambda-list ,@body)))

;; The name exporting form of this definition
(defmacro-exported defsubst-exported (function lambda-list &body body)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',function))
     (defsubst ,function ,lambda-list ,@body)))
