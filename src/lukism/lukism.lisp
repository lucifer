(in-package lukism)
#|
(eval-when (:load-toplevel :execute)
  (with-open-file (s "D:/test.com" :direction :output
		     :element-type 'unsigned-byte :if-exists :supersede)
    (bitblock:bitblock-dump
     (lukism-code-seg x86
		      (mov (% ah) ($ #x00))
		      (int ($ #x16))
		      (int ($ #x20))
		      (int ($ 3))
		      (into)
		      )
     s)))

|#
