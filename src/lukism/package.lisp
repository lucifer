(in-package cl-user)

(require 'lucifer-lamack)
(require 'cl-unification)

(defpackage lucifer.lukism
  (:nicknames lukism)
  (:use :cl :lucifer.lutilities :lucifer.lancer
	:lucifer.lamack :cl-unification)
  )

(in-package lukism)

(defparameter *code-seg-fn* nil)

(defun reintern-symbol (s)
  (intern (symbol-name s) (find-package 'lukism)))

(defun reintern-tree (tree)
  (cond
    ((symbolp tree) (reintern-symbol tree))
    ((consp tree) (cons (reintern-tree (car tree))
			(reintern-tree (cdr tree))))
    (t tree)))

(defun-exported lukism-generate-code-seg (instruction-set code-list)
  (apply (getf *code-seg-fn* (reintern-symbol instruction-set)) (reintern-tree code-list)))

(defmacro-exported lukism-code-seg (instruction-set &rest code-list)
  `(lukism-generate-code-seg ',instruction-set ',code-list))

