(in-package lukism)

(defparameter *x86-instruction-list* nil)

(defmacro define-instruction (opcode instruction)
  `(eval-when (:load-toplevel :execute)
     (setf *x86-instruction-list*
	   (nconc *x86-instruction-list* (list (list ',opcode ',instruction))))
     ))

(defmacro define-x86-instruction (opcode instruction
				  64-bitp compat-modep description)
  (declare (ignore 64-bitp compat-modep description))
  `(define-instruction ,opcode ,instruction))

;;A
(define-x86-instruction (#x37) (aaa) nil t
			"ASCII adjust AL after addition.")
(define-x86-instruction (#xD5 #x0A) (aad) nil t
			"ASCII adjust AX before division.")
(define-x86-instruction (#xD5 ?ib) (aad ?imm8) nil t
			"Adjust AX before division to number base imm8.")
(define-x86-instruction (#xD4 #x0A) (aam) nil t
			"ASCII adjust AX after multiply.")
(define-x86-instruction (#xD4 ?ib) (aam ?imm8) nil t
			"Adjust AX after multiply to number base imm8.")
(define-x86-instruction (#x3F) (aas) nil t
			"ASCII adjust AL after subtraction.")


(define-x86-instruction (#xF8) (clc) t t
			"Clear CF flag.")

(define-x86-instruction (#xFC) (cld) t t
			"Clear DF flag.")

(define-x86-instruction (#xF4) (hlt) t t
			"Halt.")

;P498
(define-x86-instruction (#xE4 ?ib) (in (% al) ?imm8) t t
			"Input byte from imm8 I/O port address into AL.")
(define-x86-instruction (#xE5 ?ib) (in (% ax) ?imm8) t t
			"Input byte from imm8 I/O port address into AX.")
(define-x86-instruction (#xE5 ?ib) (in (% eax) ?imm8) t t
			"Input byte from imm8 I/O port address into EAX.")
(define-x86-instruction (#xEC) (in (% al) (% dx)) t t
			"Input byte from I/O port in DX into AL.")
(define-x86-instruction (#xED) (in (% ax) (% dx)) t t
			"Input word from I/O port in DX into AX.")
(define-x86-instruction (#xED) (in (% eax) (% dx)) t t
			"Input doubleword from I/O port in DX into EAX.")

(define-x86-instruction (#xCC) (int ($ 3)) t t
			"Interrupt 3-trap to debugger.")
(define-x86-instruction (#xCD ?ib) (int ?imm8) t t
		"Interrupt vector number specified by immediate byte.")
(define-x86-instruction (#xCE) (into) nil t
			"Interrupt 4-if overflow flag is 1.")

(define-x86-instruction (#xC9) (leave) t t
			"Set SP to BP, then pop BP.")
(define-x86-instruction (#xC9) (leave) nil t
			"Set ESP to EBP, then pop EBP.")
(define-x86-instruction (#xC9) (leave) t nil
			"Set RSP to RBP, then pop RBP.")

(define-x86-instruction (#x90) (nop) t t
			"No operation.")

(define-x86-instruction (#x9D) (popf) t t
			"Pop top of stack into lower 16 bits of EFLAGS.")

(define-x86-instruction (#x9D) (popfd) nil t
			"Pop top of stack into EFLAGS.")

(define-x86-instruction (rex.w #x9D) (popfq) t nil
			"Pop top of stack and zero-extend into RFLAGS.")

(define-x86-instruction (#x0F #x0B) (ud2) t t
			"Raise invalid opcode exception.")

(defun x86-code-assembler-var-substitution (x unify-env)
  (if (symbolp x)
      (cond
	((eq x '?ib)  (v? '?imm (unify '($ ?imm) (v? '?imm8 unify-env))))
	)
      x))

(defun x86-code-assembler (instr)
  (or (loop with instruction-bitblock = nil
	 for instr-template in *x86-instruction-list*
	 do (setf instruction-bitblock
		  (handler-case
		      (let ((unify-env(unify (second instr-template) instr)))
			(apply
			 #'bitblock:bitblock-varsized-with-items
			 (cons 8
			       (map 'list
				    (lambda (x)
				      (x86-code-assembler-var-substitution
				       x unify-env))
				    (first instr-template)))))
		    (unification-failure () nil)
		    )
		  )
	 until instruction-bitblock
	 finally (return instruction-bitblock))
      (bitblock:bitblock-varsized-with-items 8 #x90) ;;default : NOP
      )
  )

(cl-unification:match ((list 2 '?x) (list 2 3) :errorp nil)
  ?x)

(defun x86-code-seg-generator (&rest code-list)
  (apply #'bitblock:bitblock-concatenate
	 (loop for instr in code-list
	      if (symbolp instr) ;; labels
		     do (progn nil)
		else collecting
		     (x86-code-assembler instr) into instruction-list
		finally (return instruction-list))))


(eval-when (:load-toplevel :execute)
  (locally (declare (special *code-seg-macros*))
    (setf (getf *code-seg-fn* 'x86) #'x86-code-seg-generator)))
