;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Lancer
;;; Part of Project Lucifer
;;; by CrLF0710<crlf0710@gmail.com>

;;; Lisp-function-names-tend-to-be-long-p,
;;; and Lancer will enable you to create
;;; short aliases for them.
;;; It's easy work, but must be done.
;;; Hope you like it.

;;; Note the customized syntax is inspired
;;; by the LaForth language by LaFarr,
;;; which uses a similar if..else..then syntax.

(require 'lucifer-lutilities)

(defpackage lucifer.lancer
  (:nicknames lancer)
  (:use :cl :lucifer.lutilities)
  )

(in-package :lancer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;   Lancer macros.

(defvar *lancer-classes* (make-hash-table :test #'equal))

(defun-exported ensure-lancer-class (class-name)
  (or (gethash (find-class class-name) *lancer-classes* nil)
      (setf (gethash (find-class class-name) *lancer-classes*)
	    (list nil (make-hash-table :test #'equal)))))

(defun symbol-to-keyword (symbol)
  (intern (symbol-name symbol) "KEYWORD"))

(defun-exported add-lancer-method (class-object method-name method-fn)
  (setf (gethash (symbol-to-keyword method-name)
		 (second class-object)) method-fn))


(defun-exported add-lancer-class-parent (class-name parent-class-name)
  (let ((class-object (ensure-lancer-class class-name))
	(parent-class-object (ensure-lancer-class parent-class-name)))
    (labels ((is-ancestor-or-parent-class-p (c a)
	       (and (listp c)
		    (not (null (first c)))
		    (or (eq (first c) a)
			(is-ancestor-or-parent-class-p (first c) a))))
	     (is-parent-class-p (c p)
	       (and (listp c)
		    (eq (first c) p)))
	     (is-identical-class-p (c1 c2)
	       (eq c1 c2)))
      (assert (not (is-identical-class-p class-object
					 parent-class-object)))
      (assert (not (is-ancestor-or-parent-class-p parent-class-object
						  class-object)))
      (assert (or (not (is-ancestor-or-parent-class-p class-object
						      parent-class-object))
		  (is-parent-class-p class-object parent-class-object)))
      (setf (first class-object) parent-class-object))))

(define-condition lancer-invalid-function (error)
  ((args :initarg :args)))

(defun     call-lancer-default-method (&rest args)
  (error 'lancer-invalid-function :args args))

(defun find-lancer-method (class-object method-name)
  (or (gethash method-name (second class-object))
      (and (first class-object)
	   (find-lancer-method (first class-object) method-name))
      #'call-lancer-default-method))
	   

(defun-exported call-lancer-method (class-object method-name method-args)
  (apply (find-lancer-method class-object
			     (symbol-to-keyword method-name))
	 method-args))

(declaim (inline call-lancer-method))
		
(defmacro-exported add-lancer-class-methods (class-name &body method-and-fns)
  (let ((class-sym (gensym)))
    `(eval-when (:load-toplevel :execute)
       (let ((,class-sym (lancer:ensure-lancer-class ',class-name)))
	 ,@(loop for item in method-and-fns collecting
		(destructuring-bind (method-name method-fn) item
		  `(lancer:add-lancer-method ,class-sym
					     ',method-name
					     ,method-fn)))))))

(defmacro-exported define-lancer-method (class-and-fn-name fn-name &rest args)
  (let ((class-name (if (consp class-and-fn-name)
			(first class-and-fn-name)
			class-and-fn-name))
	(class-fn-name (if (consp class-and-fn-name)
			   (second class-and-fn-name)
			   fn-name)))
    `(prog1 (defmethod ,fn-name ,@args)
       (eval-when (:compile-toplevel :load-toplevel :execute)
	 (export ',fn-name))
       (eval-when (:load-toplevel :execute)
	 (lancer:add-lancer-class-methods
	     ,class-name (,class-fn-name (fdefinition ',fn-name)))))))

(defmacro-exported call-lancer-class-method (object method-name &rest method-args)
  (let ((class-sym (gensym))
	(object-sym (gensym)))
    `(let* ((,object-sym ,object)
	    (,class-sym (lancer:ensure-lancer-class
			 (class-name (class-of ,object-sym)))))
       (lancer:call-lancer-method ,class-sym ',method-name
				  (list ,object-sym ,@method-args)))))

;; the customized syntax
(defvar *sov-mode* nil)
(defvar *lancer-reader-macro-rollback* nil)

(define-condition lancer-splitter-expected (error)
  ())

(defun lancer-reader-macro-fn (stream char &optional (rp t))
  (let* ((eof-sym (gensym))
	 (next-char (peek-char nil stream nil eof-sym rp)))
    (if (eql next-char #\[)
	(let* ((ignored-char (read-char stream t eof-sym rp))
	       (object-expr (read stream t nil rp))
	       (splitter (read stream t nil rp)))
	  (declare (ignore ignored-char))
	  (if (not (and (symbolp splitter)
			(equal (symbol-name splitter) "][")))
	      (error 'lancer-splitter-expected ))
	  (let ((args-list
		 (read-delimited-list #\] stream t)))
	    (assert (eql (read-char stream t nil t) #\)))
	    `(lancer:call-lancer-class-method
	      ,object-expr
	      ,(if (not *sov-mode*) (car args-list) (car (last args-list)))
	      ,@(if (not *sov-mode*) (cdr args-list) (butlast args-list)))))
	(if *lancer-reader-macro-rollback*
	    (funcall *lancer-reader-macro-rollback* stream char)))))

(define-condition lancer-syntax-already-enabled (error)
  ())

(define-condition lancer-syntax-not-enabled (error)
  ())

(defun-exported lancer-syntax-on (&optional (sov-mode nil))
  (when (not *lancer-reader-macro-rollback*)
    (setf *lancer-reader-macro-rollback* (get-macro-character #\( ))
    (if (not *lancer-reader-macro-rollback*)
	(error 'lancer-syntax-not-enabled))
    (set-macro-character #\( #'lancer-reader-macro-fn))
  (setf *sov-mode* sov-mode)
  t
  )

(defmacro-exported enable-lancer-syntax (&optional (sov-mode nil))
  `(eval-when (:load-toplevel :compile-toplevel :execute)
     (lancer:lancer-syntax-on ,sov-mode)))

(defun-exported lancer-syntax-off ()
  (if (not *lancer-reader-macro-rollback*)
      (error 'lancer-syntax-not-enabled))
  (set-macro-character #\( *lancer-reader-macro-rollback*)
  (setf *lancer-reader-macro-rollback* nil)
  t
  )

(defmacro-exported disable-lancer-syntax ()
  `(eval-when (:load-toplevel :compile-toplevel :execute)
     (lancer:lancer-syntax-off)))  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Experimental.

(defun lancer-print-classes ()
  (loop for class-symbol being each hash-key in *lancer-classes*
       do (format t "~a~%" class-symbol)))

(defmacro-exported register-builtin-arithmetic ()
  ;;just a demo and far from finished. Will be glad if you can help.
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (lancer:add-lancer-class-methods fixnum ;;integer
       (+ #'+)
       (- #'-)
       (* #'*)
       (/ #'/)
       )
     #+ccl
     (lancer:add-lancer-class-methods bit ;;bit
       (+ #'+)
       (- #'-)
       (* #'*)
       (/ #'/)
       )
     t
     ))

#|
(lancer-print-classes)
(loop for fun-symbol being each hash-key in (gethash 'bitblock::bitblock *lancer-classes*)
   do (format t "~a~%" (type-of fun-symbol)))
|#
